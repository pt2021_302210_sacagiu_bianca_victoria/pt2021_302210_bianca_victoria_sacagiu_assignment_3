package bll;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public interface Validator<T> {

    public int validate(T t);
}

