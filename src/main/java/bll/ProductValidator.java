package bll;

import model.Product;

import javax.swing.*;
import java.util.regex.Pattern;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ProductValidator implements Validator<Product>{
    private static final String PRICE_PATTERN = "[0-9]+";

    /**
     * @return: void
     * @param: t
     * Aceasta metoda verifica daca pretul este format doar din cifre
     */

    public int validate(Product t) {
        Pattern pattern = Pattern.compile(PRICE_PATTERN);
        if (!pattern.matcher(String.valueOf(t.getPrice())).matches()) {
            JOptionPane.showMessageDialog(null, "Not a valid price!", "Error!",
                    JOptionPane.ERROR_MESSAGE);
        }
        return 1;
    }

}
