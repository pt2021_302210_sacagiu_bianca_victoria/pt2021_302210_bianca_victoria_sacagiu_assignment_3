package bll;

import dao.ComandaDAO;
import dao.ProductDAO;
import model.Client;
import model.Comanda;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ProductBLL {
    private ProductDAO productDAO;
    private List<Validator<Product>> validators;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductValidator());
        productDAO = new ProductDAO();
    }

    /**
     * @return: Product
     * @param: id
     * Aceasta metoda returneaza produsul care are ID ul id
     */
    public Product findById(int id) {
        Product st = productDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    /**
     * @return: void
     * @param: product
     * Aceasta metoda adauga un produs in baza de date
     */
    public void insert(Product product){
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        productDAO.insert(product);
    }

    /**
     * @return: void
     * @param: id
     * Aceasta metoda sterge produsul cu ID ul id din baza de date
     */
    public void delete(int id){
        productDAO.deleteByID(id);
    }

    /**
     * @return: void
     * @param: f1, f2, id
     * Aceasta metoda modifica campul f1 in f2 al produsului cu ID ul id
     */
    public void edit(String f1, String f2, int id){
        productDAO.edit(f1, f2, id);
    }

    /**
     * @return: ArrayList<Produs>
     * @param:
     * Aceasta metoda returneaza lista de produse din baza de date
     */
    public ArrayList<Product> showAll(){
        List<Product> list = new ArrayList<>();
        list =  productDAO.showAll();
        return (ArrayList<Product>) list;
    }
}
