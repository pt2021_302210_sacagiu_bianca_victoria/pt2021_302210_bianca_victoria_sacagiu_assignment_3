package bll;

import model.Client;

import javax.swing.*;
import java.util.regex.Pattern;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ClientValidator implements Validator<Client>{
    private static final String PHONE_PATTERN = "[0-9]{10}";
    /**
     * @return: void
     * @param: t
     * Aceasta metoda verifica daca phone number are exact 10 cifre
     */
    public int validate(Client t) {
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        if (!pattern.matcher(t.getPhoneNumber()).matches()) {
            JOptionPane.showMessageDialog(null, "Not a valid phone number!", "Error!",
                    JOptionPane.ERROR_MESSAGE);
            return -1;
        }
        return 1;
    }
}
