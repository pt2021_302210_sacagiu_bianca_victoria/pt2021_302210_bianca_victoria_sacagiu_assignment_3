package bll;
import dao.ClientDAO;
import model.Client;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */
public class ClientBLL {
    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new ClientValidator());
        clientDAO = new ClientDAO();
    }
    /**
     * @return: Client
     * @param: id
     * Aceasta metoda returneaza clientul care are ID ul id
     */
    public Client findById(int id) {
        Client st = clientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return st;
    }

    /**
     * @return: void
     * @param: client
     * Aceasta metoda adauga un client in baza de date
     */
    public void insert(Client client){
        for (Validator<Client> v : validators) {
            if (v.validate(client) == -1) return;
        }
        clientDAO.insert(client);
    }
    /**
     * @return: void
     * @param: id
     * Aceasta metoda sterge clientul cu ID ul id din baza de date
     */
    public void delete(int id){
        clientDAO.deleteByID(id);
    }

    /**
     * @return: void
     * @param: f1, f2, id
     * Aceasta metoda modifica campul f1 in f2 al clientului cu ID ul id
     */
    public void edit(String f1, String f2, int id){
        clientDAO.edit(f1, f2, id);
    }

    /**
     * @return: ArrayList<Client>
     * @param:
     * Aceasta metoda returneaza lista de Clienti din baza de date
     */
    public ArrayList<Client> showAll(){
        List<Client> list = new ArrayList<>();
        list = clientDAO.showAll();
        return (ArrayList<Client>) list;
    }
}
