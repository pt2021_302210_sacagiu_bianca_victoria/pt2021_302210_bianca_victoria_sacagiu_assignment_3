package bll;
import dao.ComandaDAO;
import model.Comanda;
import model.Product;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ComandaBLL {
    private ComandaDAO comandaDAO;
    private List<Validator<Comanda>> validators;
    public ComandaBLL() {
        validators = new ArrayList<Validator<Comanda>>();
        validators.add(new OrderValidator());
        comandaDAO = new ComandaDAO();
    }

    /**
     * @return: Comanda
     * @param: id
     * Aceasta metoda returneaza comanda care are ID ul id
     */
    public  Comanda findStudentById(int id) {
        Comanda st =  comandaDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The student with id =" + id + " was not found!");
        }
        return st;
    }

    /**
     * @return: void
     * @param: comanda
     * Aceasta metoda adauga o comanda in baza de date
     */
    public void insert(Comanda comanda){
        for (Validator<Comanda> v : validators) {
            v.validate(comanda);
        }
        comandaDAO.insert(comanda);
    }

    /**
     * @return: void
     * @param: id
     * Aceasta metoda sterge comanda cu ID ul id din baza de date
     */
    public void delete(int id){
        comandaDAO.deleteByID(id);
    }

    /**
     * @return: void
     * @param: f1, f2, id
     * Aceasta metoda modifica campul f1 in f2 al comenzii cu ID ul id
     */
    public void edit(String f1, String f2, int id){
        comandaDAO.edit(f1, f2, id);
    }
    /**
     * @return: ArrayList<Comanda>
     * @param:
     * Aceasta metoda returneaza lista de comenzi din baza de date
     */
    public ArrayList<Comanda> showAll(){
        List<Comanda> list = new ArrayList<>();
        list =  comandaDAO.showAll();
        return (ArrayList<Comanda>) list;
    }
}
