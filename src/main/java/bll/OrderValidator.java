package bll;

import model.Comanda;

import javax.swing.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class OrderValidator implements Validator<Comanda> {
    private static final int MIN_Q = 1;
    private static final int MAX_Q = 30;

    /**
     * @return: void
     * @param: t
     * Aceasta metoda verifica daca cantitatea este intre 1 si 30
     */

    public int validate(Comanda t) {

        if (t.getQuantity() < MIN_Q || t.getQuantity() > MAX_Q) {
            JOptionPane.showMessageDialog(null, "Not a valid quantity!", "Error!",
                    JOptionPane.ERROR_MESSAGE);
        }
        return 1;
    }
}
