package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewEditClient extends JFrame{
    JPanel contentPane;
    JLabel title;
    JLabel etFirstName;
    JLabel etLastName;
    JLabel etPhoneNumber;
    JLabel etID;
    JTextField txtID;
    JTextField txtFirstName;
    JTextField txtLastName;
    JTextField txtPhoneNumber;
    JButton btn;
    JButton btnBack;
    public ViewEditClient(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(10, 10, 350, 410);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        title = new JLabel("EDIT CLIENT");
        title.setBounds(105,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);
        etFirstName = new JLabel("New First Name:");
        etFirstName.setBounds(10, 120, 200, 30);
        etFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etFirstName);
        txtFirstName = new JTextField();
        txtFirstName.setBounds(160, 120, 140, 30);
        txtFirstName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtFirstName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtFirstName);

        etID = new JLabel("ID:");
        etID.setBounds(10, 80, 200, 30);
        etID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etID);
        txtID = new JTextField();
        txtID.setBounds(160, 75, 140, 30);
        txtID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtID);

        etLastName = new JLabel("New Last Name:");
        etLastName.setBounds(10, 170, 200, 30);
        etLastName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etLastName);
        txtLastName = new JTextField();
        txtLastName.setBounds(160, 170, 140, 30);
        txtLastName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtLastName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtLastName);

        etPhoneNumber = new JLabel("New Phone Number:");
        etPhoneNumber.setBounds(10, 220, 200, 30);
        etPhoneNumber.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etPhoneNumber);
        txtPhoneNumber = new JTextField();
        txtPhoneNumber.setBounds(160, 220, 140, 30);
        txtPhoneNumber.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtPhoneNumber.setBackground(new Color(200, 100, 1));
        contentPane.add(txtPhoneNumber);

        btn = new JButton("EDIT");
        btn.setBounds(130, 290, 80, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);

        btnBack = new JButton("Back");
        btnBack.setBounds(240, 330, 80, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtnBack() {
        return btnBack;
    }
    public JButton getBtn() {
        return btn;
    }
    public String getNewFirstName(){
        return txtFirstName.getText();
    }
    public String getNewLastName(){
        return txtLastName.getText();
    }
    public String getNewPhoneNumber(){
        return txtPhoneNumber.getText();
    }
    public int getID(){
        return Integer.parseInt(txtID.getText());
    }

}
