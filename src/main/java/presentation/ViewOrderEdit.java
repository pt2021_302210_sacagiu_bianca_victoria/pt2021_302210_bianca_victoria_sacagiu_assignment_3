package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewOrderEdit extends JFrame {
    JPanel contentPane;
    JLabel title;
    JLabel etClientID;
    JLabel etProductID;
    JLabel etQuantity;
    JLabel etID;
    JTextField txtID;
    JTextField txtClientID;
    JTextField txtProductID;
    JTextField txtQuantity;
    JButton btn;
    JButton btnBack;
    public ViewOrderEdit(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(10, 10, 350, 410);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        title = new JLabel("EDIT ORDER");
        title.setBounds(105,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);
        etClientID = new JLabel("New Client ID:");
        etClientID.setBounds(10, 120, 200, 30);
        etClientID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etClientID);
        txtClientID = new JTextField();
        txtClientID.setBounds(160, 120, 140, 30);
        txtClientID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtClientID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtClientID);

        etID = new JLabel("ID:");
        etID.setBounds(10, 80, 200, 30);
        etID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etID);
        txtID = new JTextField();
        txtID.setBounds(160, 75, 140, 30);
        txtID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtID);

        etProductID = new JLabel("New Product ID:");
        etProductID.setBounds(10, 170, 200, 30);
        etProductID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etProductID);
        txtProductID = new JTextField();
        txtProductID.setBounds(160, 170, 140, 30);
        txtProductID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtProductID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtProductID);

        etQuantity = new JLabel("New Quantity:");
        etQuantity.setBounds(10, 220, 200, 30);
        etQuantity.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etQuantity);
        txtQuantity = new JTextField();
        txtQuantity.setBounds(160, 220, 140, 30);
        txtQuantity.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtQuantity.setBackground(new Color(200, 100, 1));
        contentPane.add(txtQuantity);

        btn = new JButton("EDIT");
        btn.setBounds(130, 290, 80, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);

        btnBack = new JButton("Back");
        btnBack.setBounds(240, 330, 80, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtnBack() {
        return btnBack;
    }
    public JButton getBtn() {
        return btn;
    }
    public int getProductID(){
        return Integer.parseInt(txtProductID.getText());
    }
    public int getID(){
        return Integer.parseInt(txtID.getText());
    }
    public int getClientID(){
        return Integer.parseInt(txtClientID.getText());
    }
    public int getQuantity(){
        return Integer.parseInt(txtQuantity.getText());
    }
}
