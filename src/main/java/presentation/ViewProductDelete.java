package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewProductDelete extends JFrame{
    JPanel contentPane;
    JLabel title;
    JLabel etID;
    JTextField txtID;
    JButton btn;
    JButton btnBack;
    public ViewProductDelete(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 250, 270);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        title = new JLabel("DELETE PRODUCT");
        title.setBounds(35,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);

        etID = new JLabel("ID:");
        etID.setBounds(20, 70, 200, 30);
        etID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etID);
        txtID = new JTextField();
        txtID.setBounds(50, 70, 90, 30);
        txtID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtID);

        btn = new JButton("DELETE");
        btn.setBounds(55, 140, 110, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);
        contentPane.setBackground(Color.GRAY);

        btnBack = new JButton("Back");
        btnBack.setBounds(120, 180, 100, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtn() {
        return btn;
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public int getID(){
        return Integer.parseInt(txtID.getText());
    }
}
