package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewAddClient extends JFrame{
    JPanel contentPane;
    JLabel title;
    JLabel etFirstName;
    JLabel etLastName;
    JLabel etPhoneNumber;
    JTextField txtFirstName;
    JTextField txtLastName;
    JTextField txtPhoneNumber;
    JButton btn;
    JButton btnBack;
    public ViewAddClient(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 350, 370);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        title = new JLabel("ADD CLIENT");
        title.setBounds(105,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);
        etFirstName = new JLabel("First Name:");
        etFirstName.setBounds(10, 70, 200, 30);
        etFirstName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etFirstName);
        txtFirstName = new JTextField();
        txtFirstName.setBounds(130, 70, 140, 30);
        txtFirstName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtFirstName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtFirstName);

        etLastName = new JLabel("Last Name:");
        etLastName.setBounds(10, 120, 200, 30);
        etLastName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etLastName);
        txtLastName = new JTextField();
        txtLastName.setBounds(130, 120, 140, 30);
        txtLastName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtLastName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtLastName);

        etPhoneNumber = new JLabel("Phone Number:");
        etPhoneNumber.setBounds(10, 170, 200, 30);
        etPhoneNumber.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etPhoneNumber);
        txtPhoneNumber = new JTextField();
        txtPhoneNumber.setBounds(130, 170, 140, 30);
        txtPhoneNumber.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtPhoneNumber.setBackground(new Color(200, 100, 1));
        contentPane.add(txtPhoneNumber);

        btn = new JButton("ADD");
        btn.setBounds(130, 240, 80, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);
        contentPane.setBackground(Color.GRAY);

        btnBack = new JButton("Back");
        btnBack.setBounds(240, 280, 80, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtn() {
        return btn;
    }

    public JButton getBtnBack() {
        return btnBack;
    }
     public String getFirstName(){
        return txtFirstName.getText();
     }
    public String getLastName(){
        return txtLastName.getText();
    }
    public String getPhoneNumber(){
        return txtPhoneNumber.getText();
    }
}
