package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewProductAdd extends JFrame{
    JPanel contentPane;
    JLabel title;
    JLabel etName;
    JLabel etPrice;
    JLabel etQuantity;
    JTextField txtName;
    JTextField txtPrice;
    JTextField txtQuantity;
    JButton btn;
    JButton btnBack;
    public ViewProductAdd(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 350, 370);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        title = new JLabel("ADD PRODUCT");
        title.setBounds(105,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);
        etName = new JLabel("Name:");
        etName.setBounds(10, 70, 200, 30);
        etName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etName);
        txtName = new JTextField();
        txtName.setBounds(130, 70, 140, 30);
        txtName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtName);

        etPrice = new JLabel("Price:");
        etPrice.setBounds(10, 120, 200, 30);
        etPrice.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etPrice);
        txtPrice = new JTextField();
        txtPrice.setBounds(130, 120, 140, 30);
        txtPrice.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtPrice.setBackground(new Color(200, 100, 1));
        contentPane.add(txtPrice);

        etQuantity = new JLabel("Quantity:");
        etQuantity.setBounds(10, 170, 200, 30);
        etQuantity.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etQuantity);
        txtQuantity = new JTextField();
        txtQuantity.setBounds(130, 170, 140, 30);
        txtQuantity.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtQuantity.setBackground(new Color(200, 100, 1));
        contentPane.add(txtQuantity);

        btn = new JButton("ADD");
        btn.setBounds(130, 240, 80, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);
        contentPane.setBackground(Color.GRAY);

        btnBack = new JButton("Back");
        btnBack.setBounds(240, 280, 80, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtn() {
        return btn;
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public String getName(){
        return txtName.getText();
    }
    public int getQuantity(){
        return Integer.parseInt(txtQuantity.getText());
    }
    public int getPrice(){
        return Integer.parseInt(txtPrice.getText());
    }
}
