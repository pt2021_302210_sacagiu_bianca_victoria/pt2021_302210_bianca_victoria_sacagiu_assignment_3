package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class View1 extends JFrame{
    JPanel contentPane;
    JButton btnClientOperations;
    JButton btnProductOperations;
    JButton btnOrderOperations;
    JLabel etTitlu;
    public View1(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 250);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        btnClientOperations = new JButton("Client Operations");
        btnClientOperations.setBounds(50, 110, 200, 50);
        btnClientOperations.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnClientOperations.setBackground(new Color(200, 100, 1));
        btnProductOperations = new JButton("Product Operations");
        btnProductOperations.setBounds(300, 110, 200, 50);
        btnProductOperations.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnProductOperations.setBackground(new Color(200, 100, 1));
        btnOrderOperations = new JButton("Order Operations");
        btnOrderOperations.setBounds(550, 110, 200, 50);
        btnOrderOperations.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnOrderOperations.setBackground(new Color(200, 100, 1));
        etTitlu = new JLabel("ORDER MANAGEMENT");
        etTitlu.setBounds(230, 30, 400, 50);
        etTitlu.setFont(new Font("Times New Roman", Font.BOLD, 30));
        contentPane.add(btnProductOperations);
        contentPane.add(btnClientOperations);
        contentPane.add(btnOrderOperations);
        contentPane.add(etTitlu);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtnClientOperations() {
        return btnClientOperations;
    }

    public JButton getBtnOrderOperations() {
        return btnOrderOperations;
    }

    public JButton getBtnProductOperations() {
        return btnProductOperations;
    }
}
