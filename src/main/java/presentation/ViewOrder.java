package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewOrder extends JFrame {
    JPanel contentPane;
    JButton btnAddOrder;
    JButton btnDeleteOrder;
    JButton btnViewOrder;
    JButton btnEditOrder;
    JLabel etTitle;
    JButton btnBack;
    public ViewOrder(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 280);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        etTitle = new JLabel("ORDER OPERATIONS");
        etTitle.setBounds(230, 30, 400, 50);
        etTitle.setFont(new Font("Times New Roman", Font.BOLD, 30));
        btnAddOrder = new JButton("Add");
        btnAddOrder.setBounds(50, 110, 150, 50);
        btnAddOrder.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnAddOrder.setBackground(new Color(200, 100, 1));
        btnDeleteOrder = new JButton("Delete");
        btnDeleteOrder.setBounds(230, 110, 150, 50);
        btnDeleteOrder.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnDeleteOrder.setBackground(new Color(200, 100, 1));
        btnEditOrder = new JButton("Edit");
        btnEditOrder.setBounds(410, 110, 150, 50);
        btnEditOrder.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnEditOrder.setBackground(new Color(200, 100, 1));
        btnViewOrder = new JButton("Show all");
        btnViewOrder.setBounds(580, 110, 156, 50);
        btnViewOrder.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnViewOrder.setBackground(new Color(200, 100, 1));
        btnBack = new JButton("Back");
        btnBack.setBounds(640, 180, 120, 40);
        btnBack.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.add(btnAddOrder);
        contentPane.add(btnDeleteOrder);
        contentPane.add(btnEditOrder);
        contentPane.add(btnViewOrder);
        contentPane.setBackground(Color.GRAY);
        contentPane.add(etTitle);
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getBtnAddOrder() {
        return btnAddOrder;
    }

    public JButton getBtnDeleteOrder() {
        return btnDeleteOrder;
    }

    public JButton getBtnEditOrder() {
        return btnEditOrder;
    }

    public JButton getBtnViewOrder() {
        return btnViewOrder;
    }
}
