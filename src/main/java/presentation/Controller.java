package presentation;
import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProductBLL;
import model.Client;
import model.Comanda;
import model.Product;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class Controller {
    View1 view= new View1();
    ViewClient viewClient = new ViewClient();
    ViewProduct viewProduct = new ViewProduct();
    ViewOrder viewOrder = new ViewOrder();
    ViewAddClient viewAddClient;
    ViewDeleteClient viewDeleteClient;
    ViewEditClient viewEditClient;
    ViewProductAdd viewProductAdd;
    ViewProductDelete viewProductDelete;
    ViewProductEdit viewProductEdit;
    ViewOrderAdd viewOrderAdd;
    ViewOrderDelete viewOrderDelete;
    ViewOrderEdit viewOrderEdit;
    ViewOrderShowAll viewOrderShowAll;
    ViewClientShowAll viewClientShowAll;
    ViewProductShowAll viewProductShowAll;
    ClientBLL clientBll = new ClientBLL();
    ProductBLL productBLL = new ProductBLL();
    ComandaBLL comandaBLL = new ComandaBLL();
    public Controller(){
        view.setVisible(true);
        view.getBtnClientOperations().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewClient.setVisible(true);
                view.setVisible(false);
            }
        });
        viewClient.getBtnAddClient().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewAddClient = new ViewAddClient();
                viewAddClient.setVisible(true);
                viewClient.setVisible(false);
                viewAddClient.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clientBll.insert(new Client(viewAddClient.getFirstName(), viewAddClient.getLastName(), viewAddClient.getPhoneNumber()));
                    }
                });
                viewAddClient.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewClient.setVisible(true);
                        viewAddClient.setVisible(false);
                        viewAddClient.dispose();
                    }
                });

            }
        });
        view.getBtnProductOperations().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProduct.setVisible(true);
                view.setVisible(false);
            }
        });
        view.getBtnOrderOperations().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewOrder.setVisible(true);
                view.setVisible(false);
            }
        });
        viewClient.getBtnEditClient().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewEditClient = new ViewEditClient();
                viewEditClient.setVisible(true);
                viewClient.setVisible(false);
                viewEditClient.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clientBll.edit("firstName", viewEditClient.getNewFirstName(), viewEditClient.getID());
                        clientBll.edit("lastName", viewEditClient.getNewLastName(), viewEditClient.getID());
                        clientBll.edit("phoneNumber", viewEditClient.getNewPhoneNumber(), viewEditClient.getID());
                    }
                });
                viewEditClient.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewEditClient.setVisible(false);
                        viewEditClient.dispose();
                        viewClient.setVisible(true);
                    }
                });
            }
        });
        viewClient.getBtnDeleteClient().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewDeleteClient = new ViewDeleteClient();
                viewClient.setVisible(false);
                viewDeleteClient.setVisible(true);
                viewDeleteClient.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        clientBll.delete(Integer.parseInt(viewDeleteClient.getId()));
                    }
                });
                viewDeleteClient.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewDeleteClient.setVisible(false);
                        viewDeleteClient.dispose();
                        viewClient.setVisible(true);
                    }
                });
            }
        });
        viewClient.getBtnBack().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewClient.setVisible(false);
                view.setVisible(true);
            }
        });
        viewProduct.getBtnAddProduct().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProductAdd = new ViewProductAdd();
                viewProductAdd.setVisible(true);
                viewProduct.setVisible(false);
                viewProductAdd.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        productBLL.insert(new Product(viewProductAdd.getName(), viewProductAdd.getQuantity(),viewProductAdd.getPrice()));
                    }
                });
                viewProductAdd.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewProductAdd.dispose();
                        viewProduct.setVisible(true);
                    }
                });
            }
        });
        viewProduct.getBtnDeleteProduct().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProductDelete = new ViewProductDelete();
                viewProductDelete.setVisible(true);
                viewProduct.setVisible(false);
                viewProductDelete.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        productBLL.delete(viewProductDelete.getID());
                    }
                });
                viewProductDelete.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewProductDelete.dispose();
                        viewProduct.setVisible(true);
                    }
                });
            }
        });
        viewProduct.getBtnBack().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProduct.setVisible(false);
                view.setVisible(true);
            }
        });
        viewProduct.getBtnEditProduct().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProductEdit = new ViewProductEdit();
                viewProductEdit.setVisible(true);
                viewProduct.setVisible(false);
                viewProductEdit.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        productBLL.edit("name", viewProductEdit.getName(), viewProductEdit.getID());
                        productBLL.edit("price", viewProductEdit.getPrice(), viewProductEdit.getID());
                        productBLL.edit("quantity", viewProductEdit.getQuantity(), viewProductEdit.getID());
                    }
                });
                viewProductEdit.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewProductEdit.dispose();
                        viewProduct.setVisible(true);
                    }
                });
            }
        });
        viewOrder.getBtnBack().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                view.setVisible(true);
                viewOrder.setVisible(false);
            }
        });
        viewOrder.getBtnAddOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewOrderAdd = new ViewOrderAdd();
                viewOrder.setVisible(false);
                viewOrderAdd.setVisible(true);
                viewOrderAdd.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(productBLL.findById(viewOrderAdd.getProductID()).getQuantity() <
                                viewOrderAdd.getQuantity()) {
                            String s = "There are only " +
                                    productBLL.findById(viewOrderAdd.getProductID()).getQuantity() + " pieces left!";
                            JOptionPane.showMessageDialog(null, s, "Error!",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        else {
                            Comanda comanda = new Comanda(viewOrderAdd.getClientID(), viewOrderAdd.getProductID(), viewOrderAdd.getQuantity());
                            comanda.setPrice(viewOrderAdd.getQuantity() *
                                    productBLL.findById(viewOrderAdd.getProductID()).getPrice());
                            int q = productBLL.findById(viewOrderAdd.getProductID()).getQuantity() - viewOrderAdd.getQuantity();
                            productBLL.edit("quantity", String.valueOf(q), viewOrderAdd.getProductID());
                            comandaBLL.insert(comanda);
                        }
                    }
                });
                viewOrderAdd.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewOrderAdd.dispose();
                        viewOrder.setVisible(true);
                    }
                });
            }
        });
        viewOrder.getBtnDeleteOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewOrderDelete = new ViewOrderDelete();
                viewOrder.setVisible(false);
                viewOrderDelete.setVisible(true);
                viewOrderDelete.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        productBLL.edit("quantity", String.valueOf(productBLL.findById(comandaBLL.findStudentById(viewOrderDelete.getID()).getProductID()).getQuantity()
                                            + comandaBLL.findStudentById(viewOrderDelete.getID()).getQuantity()), productBLL.findById(comandaBLL.findStudentById(viewOrderDelete.getID()).getProductID()).getID());
                        comandaBLL.delete(viewOrderDelete.getID());
                    }
                });
                viewOrderDelete.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewOrderDelete.dispose();
                        viewOrder.setVisible(true);
                    }
                });
            }
        });
        viewOrder.getBtnEditOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewOrderEdit = new ViewOrderEdit();
                viewOrder.setVisible(false);
                viewOrderEdit.setVisible(true);
                viewOrderEdit.getBtn().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if(productBLL.findById(viewOrderEdit.getProductID()).getQuantity() +
                                comandaBLL.findStudentById(viewOrderEdit.getID()).getQuantity() <
                                viewOrderEdit.getQuantity()) {
                            int i = productBLL.findById(viewOrderEdit.getProductID()).getQuantity() +
                                    comandaBLL.findStudentById(viewOrderEdit.getID()).getQuantity();
                            String s = "Insufficient Stock!";
                            JOptionPane.showMessageDialog(null, s, "Error!",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        else {
                            comandaBLL.edit("clientID", String.valueOf(viewOrderEdit.getClientID()), viewOrderEdit.getID());
                            comandaBLL.edit("productID", String.valueOf(viewOrderEdit.getProductID()), viewOrderEdit.getID());
                            productBLL.edit("quantity", String.valueOf(productBLL.findById(viewOrderEdit.getProductID()).getQuantity() +
                                    comandaBLL.findStudentById(viewOrderEdit.getID()).getQuantity()), viewOrderEdit.getProductID());
                            productBLL.edit("quantity", String.valueOf(productBLL.findById(viewOrderEdit.getProductID()).getQuantity() -
                                    viewOrderEdit.getQuantity()), viewOrderEdit.getProductID());
                            comandaBLL.edit("price", String.valueOf(viewOrderEdit.getQuantity() *
                                    productBLL.findById(viewOrderEdit.getProductID()).getPrice()), viewOrderEdit.getID());
                            comandaBLL.edit("quantity", String.valueOf(viewOrderEdit.getQuantity()), viewOrderEdit.getID());
                        }
                    }
                });
                viewOrderEdit.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewOrderEdit.dispose();
                        viewOrder.setVisible(true);
                    }
                });
            }
        });
        viewClient.getBtnViewClients().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewClientShowAll = new ViewClientShowAll(clientBll.showAll());
                viewClientShowAll.setVisible(true);
                viewClient.setVisible(false);
                viewClientShowAll.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewClientShowAll.dispose();
                        viewClient.setVisible(true);
                    }
                });
            }
        });
        viewProduct.getBtnViewProduct().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewProductShowAll = new ViewProductShowAll(productBLL.showAll());
                viewProductShowAll.setVisible(true);
                viewProduct.setVisible(false);
                viewProductShowAll.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewProductShowAll.dispose();
                        viewProduct.setVisible(true);
                    }
                });
            }
        });
        viewOrder.getBtnViewOrder().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewOrderShowAll = new ViewOrderShowAll(comandaBLL.showAll());
                viewOrder.setVisible(false);
                viewOrderShowAll.setVisible(true);
                viewOrderShowAll.getBtnBack().addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        viewOrder.setVisible(true);
                        viewOrderShowAll.dispose();
                    }
                });
            }
        });
    }
}
