package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewProduct extends JFrame {
    JPanel contentPane;
    JButton btnAddProduct;
    JButton btnDeleteProduct;
    JButton btnViewProduct;
    JButton btnEditProduct;
    JLabel etTitle;
    JButton btnBack;
    public ViewProduct(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 280);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        etTitle = new JLabel("PRODUCT OPERATIONS");
        etTitle.setBounds(230, 30, 400, 50);
        etTitle.setFont(new Font("Times New Roman", Font.BOLD, 30));
        btnAddProduct = new JButton("Add");
        btnAddProduct.setBounds(50, 110, 150, 50);
        btnAddProduct.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnAddProduct.setBackground(new Color(200, 100, 1));
        btnDeleteProduct = new JButton("Delete");
        btnDeleteProduct.setBounds(230, 110, 150, 50);
        btnDeleteProduct.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnDeleteProduct.setBackground(new Color(200, 100, 1));
        btnEditProduct = new JButton("Edit");
        btnEditProduct.setBounds(410, 110, 150, 50);
        btnEditProduct.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnEditProduct.setBackground(new Color(200, 100, 1));
        btnViewProduct = new JButton("Show all");
        btnViewProduct.setBounds(580, 110, 156, 50);
        btnViewProduct.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnViewProduct.setBackground(new Color(200, 100, 1));
        contentPane.add(btnAddProduct);
        contentPane.add(btnDeleteProduct);
        contentPane.add(btnEditProduct);
        contentPane.add(btnViewProduct);
        contentPane.setBackground(Color.GRAY);
        contentPane.add(etTitle);
        btnBack = new JButton("Back");
        btnBack.setBounds(640, 180, 120, 40);
        btnBack.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
    }

    public JButton getBtnAddProduct() {
        return btnAddProduct;
    }

    public JButton getBtnDeleteProduct() {
        return btnDeleteProduct;
    }

    public JButton getBtnEditProduct() {
        return btnEditProduct;
    }

    public JButton getBtnViewProduct() {
        return btnViewProduct;
    }

    public JButton getBtnBack() {
        return btnBack;
    }

}
