package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewClient extends JFrame {
    JPanel contentPane;
    JButton btnAddClient;
    JButton btnDeleteClient;
    JButton btnViewClients;
    JButton btnEditClient;
    JLabel etTitle;
    JButton btnBack;
    public ViewClient(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 280);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        etTitle = new JLabel("CLIENT OPERATIONS");
        etTitle.setBounds(230, 30, 400, 50);
        etTitle.setFont(new Font("Times New Roman", Font.BOLD, 30));
        btnAddClient = new JButton("Add");
        btnAddClient.setBounds(50, 110, 150, 50);
        btnAddClient.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnAddClient.setBackground(new Color(200, 100, 1));
        btnDeleteClient = new JButton("Delete");
        btnDeleteClient.setBounds(230, 110, 150, 50);
        btnDeleteClient.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnDeleteClient.setBackground(new Color(200, 100, 1));
        btnEditClient = new JButton("Edit");
        btnEditClient.setBounds(410, 110, 150, 50);
        btnEditClient.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnEditClient.setBackground(new Color(200, 100, 1));
        btnViewClients = new JButton("Show all");
        btnViewClients.setBounds(580, 110, 156, 50);
        btnViewClients.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnViewClients.setBackground(new Color(200, 100, 1));
        contentPane.add(btnAddClient);
        btnBack = new JButton("Back");
        btnBack.setBounds(640, 180, 120, 40);
        btnBack.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.add(btnAddClient);
        contentPane.add(btnDeleteClient);
        contentPane.add(btnEditClient);
        contentPane.add(btnViewClients);
        contentPane.setBackground(Color.GRAY);
        contentPane.add(etTitle);
    }

    public JButton getBtnAddClient() {
        return btnAddClient;
    }

    public JButton getBtnDeleteClient() {
        return btnDeleteClient;
    }

    public JButton getBtnViewClients() {
        return btnViewClients;
    }

    public JButton getBtnEditClient() {
        return btnEditClient;
    }

    public JButton getBtnBack() {
        return btnBack;
    }
}
