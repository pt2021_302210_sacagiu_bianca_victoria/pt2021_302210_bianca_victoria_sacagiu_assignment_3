package presentation;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewProductEdit extends JFrame {
    JPanel contentPane;
    JLabel title;
    JLabel etName;
    JLabel etPrice;
    JLabel etQuantity;
    JLabel etID;
    JTextField txtID;
    JTextField txtName;
    JTextField txtPrice;
    JTextField txtQuantity;
    JButton btn;
    JButton btnBack;
    public ViewProductEdit(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(10, 10, 350, 410);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        title = new JLabel("EDIT PRODUCT");
        title.setBounds(105,10, 400, 50);
        title.setFont(new Font("Times New Roman", Font.BOLD, 20));
        contentPane.add(title);
        etName = new JLabel("New Name:");
        etName.setBounds(10, 120, 200, 30);
        etName.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etName);
        txtName = new JTextField();
        txtName.setBounds(160, 120, 140, 30);
        txtName.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtName.setBackground(new Color(200, 100, 1));
        contentPane.add(txtName);

        etID = new JLabel("ID:");
        etID.setBounds(10, 80, 200, 30);
        etID.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etID);
        txtID = new JTextField();
        txtID.setBounds(160, 75, 140, 30);
        txtID.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtID.setBackground(new Color(200, 100, 1));
        contentPane.add(txtID);

        etPrice = new JLabel("New Price:");
        etPrice.setBounds(10, 170, 200, 30);
        etPrice.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etPrice);
        txtPrice = new JTextField();
        txtPrice.setBounds(160, 170, 140, 30);
        txtPrice.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtPrice.setBackground(new Color(200, 100, 1));
        contentPane.add(txtPrice);

        etQuantity = new JLabel("New Quantity:");
        etQuantity.setBounds(10, 220, 200, 30);
        etQuantity.setFont(new Font("Times New Roman", Font.PLAIN, 17));
        contentPane.add(etQuantity);
        txtQuantity = new JTextField();
        txtQuantity.setBounds(160, 220, 140, 30);
        txtQuantity.setFont(new Font("Times New Roman", Font.BOLD, 18));
        txtQuantity.setBackground(new Color(200, 100, 1));
        contentPane.add(txtQuantity);

        btn = new JButton("EDIT");
        btn.setBounds(130, 290, 80, 30);
        btn.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btn.setBackground(new Color(200, 100, 1));
        contentPane.add(btn);

        btnBack = new JButton("Back");
        btnBack.setBounds(240, 330, 80, 30);
        btnBack.setFont(new Font("Times New Roman", Font.BOLD, 17));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        contentPane.setBackground(Color.GRAY);
    }

    public JButton getBtnBack() {
        return btnBack;
    }
    public JButton getBtn() {
        return btn;
    }
    public String getName(){
        return txtName.getText();
    }
    public String getPrice(){
        return txtPrice.getText();
    }
    public String getQuantity(){
        return txtQuantity.getText();
    }
    public int getID(){
        return Integer.parseInt(txtID.getText());
    }
}
