package presentation;

import model.Client;
import model.Product;
import start.ReflectionExample;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class ViewClientShowAll extends JFrame {
    JPanel contentPane;
    JLabel etTitle;
    JButton btnBack;
    JTable clientTable;
    JScrollPane scrollPane;
    public ViewClientShowAll(ArrayList<Client> list) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 340, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        contentPane.setBackground(Color.GRAY);
        etTitle = new JLabel("CLIENTS");
        etTitle.setBounds(103, 30, 400, 50);
        etTitle.setFont(new Font("Times New Roman", Font.BOLD, 30));
        contentPane.add(etTitle);
        btnBack = new JButton("Back");
        btnBack.setBounds(210, 320, 100, 30);
        btnBack.setFont(new Font("Times New Roman", Font.PLAIN, 20));
        btnBack.setBackground(new Color(200, 100, 1));
        contentPane.add(btnBack);
        scrollPane = new JScrollPane();
        scrollPane.setBounds(10, 90, 300, 200);
        contentPane.add(scrollPane);
        String[] column = {"ID", "First Name", "Last Name", "Phone Number"};
        Object[][] obj = new Object[list.size()][4];
        for(int i = 0; i < list.size(); i ++){
            obj[i][0] = list.get(i).getID();
            obj[i][1] = list.get(i).getFirstName();
            obj[i][2] = list.get(i).getLastName();
            obj[i][3] = list.get(i).getPhoneNumber();
        }
        clientTable = new JTable(obj, column);
        clientTable.setBackground(new Color(200, 100, 1));
        scrollPane.setViewportView(clientTable);
        Client client = new Client();
        clientTable = ReflectionExample.createTable(list, client);
        scrollPane.setViewportView(clientTable);
    }
    public JButton getBtnBack() {
        return btnBack;
    }
}
