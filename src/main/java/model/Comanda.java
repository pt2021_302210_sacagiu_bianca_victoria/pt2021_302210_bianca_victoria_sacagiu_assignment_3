package model;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class Comanda {
    private int ID;
    private int clientID;
    private int productID;
    private int price;
    private int quantity;

    public Comanda(){

    }
    public Comanda(int clientID, int productID, int quantity) {
        this.clientID = clientID;
        this.productID = productID;
        this.quantity = quantity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return this.price;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getID() {
        return ID;
    }

    public int getClientID() {
        return clientID;
    }

    public int getProductID() {
        return productID;
    }

    public String toString(){
        return ID + ", " + clientID + ", " + productID + ", " + price + ", " + quantity;
    }


}
