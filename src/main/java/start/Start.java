package start;
import bll.ClientBLL;
import bll.ComandaBLL;
import bll.ProductBLL;
import bll.Validator;
import model.Client;
import model.Comanda;
import presentation.Controller;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * @Author: Sacagiu Bianca-Victoria
 * @Since: Apr 19, 2021
 */

public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

    public static void main(String[] args) throws SQLException {

        Controller controller = new Controller();
        ClientBLL clientBLL = new ClientBLL();
        ProductBLL productBLL = new ProductBLL();
        ComandaBLL comandaBLL = new ComandaBLL();
        ArrayList<Comanda> list = new ArrayList<>();
        list = comandaBLL.showAll();
        try {
            FileWriter output = new FileWriter("bill.txt");
            for(int i = 0; i < list.size(); i ++) {
                output.write("BILL " + (i + 1) + "\n");
                output.write("ORDER ID: " + list.get(i).getID() + "\n");
                output.write("CLIENT ID: " + list.get(i).getClientID() + "\n");
                output.write("PRODUCT ID: " + list.get(i). getProductID() + "\n");
                output.write("QUANTITY: " + list.get(i).getQuantity() + "\n");
                output.write("PRICE: " + list.get(i).getPrice() + "\n");
                output.write("\n");
            }
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // obtain field-value pairs for object through reflection
      //  ReflectionExample.retrieveProperties(student1);

    }
}
